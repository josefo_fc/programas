package Practica;
import java.util.ArrayList;

public class Pokedex {
    private ArrayList<Pokemon> listaPokemon;
    
    public Pokedex() {
        listaPokemon = new ArrayList<>();
    }
    
    public void agregarPokemon(Pokemon pokemon) {
        listaPokemon.add(pokemon);
    }
    
    public Pokemon buscarPokemon(String nombre) {
        for (Pokemon pokemon : listaPokemon) {
            if (pokemon.getNombre().equals(nombre)) {
                return pokemon;
            }
        }
        return null;
    }

    public Pokemon buscarPokemon_id(int id_num) {
        for (Pokemon pokemon : listaPokemon) {
            if (pokemon.getId() == id_num) {
                return pokemon;
            }
        }
        return null;
    }


}
