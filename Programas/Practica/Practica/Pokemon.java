package Practica;
public class Pokemon{
    
    String nombre;
    int id_num;

    /*
     * Creamos el constructor del pokemon
     */
    public Pokemon(String nombre, int id_num){
        this.nombre = nombre;
        this.id_num = id_num;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setId(int id_num){
        this.id_num = id_num;
    }

    public String getNombre(){
        return this.nombre;
    }

    public int getId(){
        return this.id_num;
    }

}