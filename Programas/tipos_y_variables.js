
var empleado = {
    nombre: 'Alex',
    empleo: 'desarrollador',
    edad: 1,
    armas: ["pistola","metralleta", "kungfu"]
};

//convierte un objeto a cadena
var serializado = JSON.stringify(empleado);
console.log(serializado);


//Ahora lo contrario, o sea des-serializar
serializado = serializado + '}';
var leido;
var texto = '{"Nombre": "Alex", "empleo": "desarrollador"}';

// ------ EXCEPCIONES -------

try{
    leido = JSON.parse(serializado); // 
}catch (err){
    console.log("No se pude leer serializado");
}

if(typeof leido != "undefined"){
    console.log("Hay algo leido");
}else{
    console.log("No hay nada leido");
}

// ------- BUCLE FOR ----------

//Recorrer a empleado

for(var i=0; i < empleado.armas.length; i++){
    var arma = empleado.armas[i];
    console.log(empleado.nombre + (arma == "kungfu" ? " sabe " : " Tiene ") + arma);
}

// ------- FUNCIONES --------
 function hola(num){
    console.log(num);
    return "smith";
 }

 hola(1);

 function suma(num1,num2){
    return num1+num2;
 }

 console.log(suma(1,54));
